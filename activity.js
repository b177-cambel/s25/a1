// 2. Use the count operator to count the total number of fruits on sale.
db.fruits.aggregate([
    { $match: { onSale: true } },
    { $count: "Number of fruits on sale" }
]);

// 3. Use the count operator to count the total number of fruits with stock more than 20.
db.fruits.aggregate([
    { $match: { stock: { $gt: 20 } } },
    { $count: "Number of fruits with stock more than 20" }
]);

// 4. Use the average operator to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
    { $match: { onSale: true } },
    { $group: { _id: "$supplier_id", AveragePrice: { $avg: "$price" } } }
]);

// 5. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
    { $group: { _id: "$supplier_id", MaxPrice: { $max: "$price" } } }
]);

// 6. Use the min operator to get the lowest price of a fruit per supplier.

db.fruits.aggregate([
    { $group: { _id: "$supplier_id", MinPrice: { $min: "$price" } } }
]);






